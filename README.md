# busybox

Tiny utilities for small and embedded systems. [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=busybox)

* https://busybox.net

## See also
* gitlab.com/[**software-packages-demo/busybox**](https://gitlab.com/software-packages-demo/busybox)
* [gitlab.com/hub-docker-com-demo/alpine](https://gitlab.com/hub-docker-com-demo/alpine)
* gitlab.com/[apt-packages-demo/busybox](https://gitlab.com/apt-packages-demo/busybox)
* gitlab.com/[dnf-packages-demo/busybox](https://gitlab.com/dnf-packages-demo/busybox)
* gitlab.com/[xbps-packages-demo/busybox](https://gitlab.com/xbps-packages-demo/busybox)

## License
* GPLv2
* [*BusyBox is licensed under the GNU General Public License, version 2*
  ](https://busybox.net/license.html)
